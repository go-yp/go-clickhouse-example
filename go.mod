module gitlab.com/go-yp/go-clickhouse-example

go 1.16

require (
	github.com/ClickHouse/clickhouse-go v1.4.5
	github.com/stretchr/testify v1.3.0
)
