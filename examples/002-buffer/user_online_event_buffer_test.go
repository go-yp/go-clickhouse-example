package main

import (
	"github.com/stretchr/testify/require"
	"sync/atomic"
	"testing"
)

func BenchmarkUserOnlineEventBuffer(b *testing.B) {
	var buffer = NewUserOnlineEventBuffer()

	var expectedCount uint32 = 0

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var i = atomic.AddUint32(&expectedCount, 1)

			buffer.Add(UserOnlineEvent{
				Time:   int32(i),
				UserID: uint32(i),
				Role:   uint8(i % 2),
			})
		}
	})

	var (
		packs       = buffer.GetAndClear()
		actualCount = 0
	)

	for _, pack := range packs {
		actualCount += len(pack)
	}

	require.Equal(b, b.N, actualCount)
}
