package main

import (
	"sync"
	"time"
)

type UserOnlineEventWorker struct {
	buffer         *UserOnlineEventBuffer
	storage        *UserOnlineEventStorage
	flushInterval  time.Duration
	mu             sync.Mutex
	shutdownSignal chan struct{}
	shutdown       bool
}

func NewUserOnlineEventWorker(buffer *UserOnlineEventBuffer, storage *UserOnlineEventStorage, flushInterval time.Duration) *UserOnlineEventWorker {
	var worker = &UserOnlineEventWorker{
		buffer:         buffer,
		storage:        storage,
		flushInterval:  flushInterval,
		shutdownSignal: make(chan struct{}),
	}

	go worker.run()

	return worker
}

// FlushAndClose
func (w *UserOnlineEventWorker) Shutdown() {
	w.mu.Lock()
	defer w.mu.Unlock()

	if w.shutdown {
		w.store()

		return
	}

	w.shutdown = true

	w.shutdownSignal <- struct{}{}
	<-w.shutdownSignal

	close(w.shutdownSignal)

	w.store()
}

func (w *UserOnlineEventWorker) run() {
	ticker := time.NewTicker(w.flushInterval)

	for {
		select {
		case <-ticker.C:
			w.store()

		case <-w.shutdownSignal:
			ticker.Stop()

			w.shutdownSignal <- struct{}{}

			return
		}
	}
}

func (w *UserOnlineEventWorker) store() {
	packs := w.buffer.GetAndClear()

	if len(packs) == 0 {
		return
	}

	for _, pack := range packs {
		err := w.storage.Store(pack)

		if err != nil {
			// log error
		}
	}
}
