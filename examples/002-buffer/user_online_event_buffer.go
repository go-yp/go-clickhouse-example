package main

import "sync"

const (
	packSize = 10000
)

type UserOnlineEventBuffer struct {
	current       []UserOnlineEvent
	complete      [][]UserOnlineEvent
	currentLength int
	mu            sync.Mutex
}

func NewUserOnlineEventBuffer() *UserOnlineEventBuffer {
	return &UserOnlineEventBuffer{}
}

func (b *UserOnlineEventBuffer) Add(event UserOnlineEvent) {
	b.mu.Lock()
	defer b.mu.Unlock()

	b.current = append(b.current, event)
	b.currentLength += 1

	if b.isCurrentPackFull() {
		b.completeCurrentPack()
	}
}

func (b *UserOnlineEventBuffer) GetAndClear() [][]UserOnlineEvent {
	b.mu.Lock()
	defer b.mu.Unlock()

	b.completeCurrentPack()

	var result = b.complete
	b.complete = nil
	return result
}

func (b *UserOnlineEventBuffer) isCurrentPackFull() bool {
	return b.currentLength >= packSize
}

func (b *UserOnlineEventBuffer) completeCurrentPack() {
	if b.currentLength > 0 {
		b.complete = append(b.complete, b.current)

		b.current = make([]UserOnlineEvent, 0, packSize)

		b.currentLength = 0
	}
}
