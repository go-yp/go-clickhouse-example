package main

import (
	"database/sql"
	_ "github.com/ClickHouse/clickhouse-go"
	"sync/atomic"
	"testing"
	"time"
)

func BenchmarkUserOnlineEvent(b *testing.B) {
	var startTime = time.Now()

	connect, err := sql.Open("clickhouse", "tcp://clickhouse:9000?database=yaaws")
	if err != nil {
		b.Fatal(err)
	}

	if err := connect.Ping(); err != nil {
		b.Fatal(err)
	}

	if err := resetTable(connect); err != nil {
		b.Fatal(err)
	}

	var (
		buffer  = NewUserOnlineEventBuffer()
		storage = NewUserOnlineEventStorage(connect)
		worker  = NewUserOnlineEventWorker(buffer, storage, time.Second)
	)

	var counter = uint32(0)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var i = atomic.AddUint32(&counter, 1)

			buffer.Add(UserOnlineEvent{
				Time:   int32(i),
				UserID: uint32(i),
				Role:   uint8(i % 2),
			})
		}
	})

	// for graceful shutdown
	worker.Shutdown()

	b.Logf("inserted count %10d %s", b.N, time.Since(startTime))
}
