package main

type UserOnlineEventEntity struct {
	UserOnlineEvent
}

func (e UserOnlineEventEntity) Query() string {
	const (
		// language=SQL
		insertSQL = `INSERT INTO user_online_source_statistics (date, time, user_id, role)
VALUES (?, ?, ?, ?);`
	)

	return insertSQL
}

func (e *UserOnlineEventEntity) Values() []interface{} {
	return []interface{}{
		e.Time,
		e.Time,
		e.UserID,
		e.Role,
	}
}
