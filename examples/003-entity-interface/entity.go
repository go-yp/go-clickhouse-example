package main

type Entity interface {
	Query() string
	Values() []interface{}
}
