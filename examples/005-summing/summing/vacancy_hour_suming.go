package summing

import (
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/entities"
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/events"
)

func VacancyView(source events.VacancyViewEvent) (entities.VacancyHourKey, entities.VacancyHourValue) {
	return entities.VacancyHourKey{
			Time:      roundHour(source.Time),
			UserID:    source.UserID,
			VacancyID: source.VacancyID,
		}, entities.VacancyHourValue{
			ViewCount:  1,
			ApplyCount: 0,
		}
}

func VacancyApply(source events.VacancyApplyEvent) (entities.VacancyHourKey, entities.VacancyHourValue) {
	return entities.VacancyHourKey{
			Time:      roundHour(source.Time),
			UserID:    source.UserID,
			VacancyID: source.VacancyID,
		}, entities.VacancyHourValue{
			ViewCount:  0,
			ApplyCount: 1,
		}
}

func roundHour(s int32) int32 {
	return s / 3600 * 3600
}
