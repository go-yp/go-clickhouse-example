package main

import (
	"database/sql"
	_ "github.com/ClickHouse/clickhouse-go"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/buffer"
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/events"
	"sync/atomic"
	"testing"
	"time"
)

func BenchmarkEvents(b *testing.B) {
	var startTime = time.Now()

	connect, err := sql.Open("clickhouse", "tcp://clickhouse:9000?database=yaaws")
	require.NoError(b, err)

	require.NoError(b, connect.Ping())

	require.NoError(b, vacancyViewResetTable(connect))
	require.NoError(b, vacancyApplyResetTable(connect))
	require.NoError(b, vacancyHourResetTable(connect))

	var (
		storage = NewStorage(connect)

		vacancyViewBuffer = buffer.NewBuffer()
		vacancyViewWorker = NewWorker(vacancyViewBuffer, storage, time.Second)

		vacancyApplyBuffer = buffer.NewBuffer()
		vacancyApplyWorker = NewWorker(vacancyApplyBuffer, storage, time.Second)

		vacancyHourBuffer = buffer.NewVacancyHourBuffer()
		vacancyHourWorker = NewWorker(vacancyHourBuffer, storage, time.Second)

		vacancyProxyBuffer = buffer.NewVacancyProxyBuffer(
			vacancyViewBuffer,
			vacancyApplyBuffer,
			vacancyHourBuffer,
		)
	)

	var counter = uint32(0)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var i = atomic.AddUint32(&counter, 1)

			{
				var event = events.VacancyViewEvent{
					Time:      int32(i),
					UserID:    uint32(i % 32),
					VacancyID: uint32(i % 64),
				}

				vacancyProxyBuffer.AddVacancyView(event)
			}

			{
				var event = events.VacancyApplyEvent{
					Time:      int32(i),
					UserID:    uint32(i % 32),
					VacancyID: uint32(i % 64),
				}

				vacancyProxyBuffer.AddVacancyApply(event)
			}
		}
	})

	// for graceful shutdown
	vacancyViewWorker.Shutdown()
	vacancyApplyWorker.Shutdown()
	vacancyHourWorker.Shutdown()

	b.Logf("inserted count ~ %10d %s", 3*b.N, time.Since(startTime))
}
