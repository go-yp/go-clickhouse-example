package main

import (
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/buffer"
	"sync"
	"time"
)

type Worker struct {
	buffer         buffer.Getter
	storage        *Storage
	flushInterval  time.Duration
	mu             sync.Mutex
	shutdownSignal chan struct{}
	shutdown       bool
}

func NewWorker(buffer buffer.Getter, storage *Storage, flushInterval time.Duration) *Worker {
	var worker = &Worker{
		buffer:         buffer,
		storage:        storage,
		flushInterval:  flushInterval,
		shutdownSignal: make(chan struct{}),
	}

	go worker.run()

	return worker
}

// FlushAndClose
func (w *Worker) Shutdown() {
	w.mu.Lock()
	defer w.mu.Unlock()

	if w.shutdown {
		w.store()

		return
	}

	w.shutdown = true

	w.shutdownSignal <- struct{}{}
	<-w.shutdownSignal

	close(w.shutdownSignal)

	w.store()
}

func (w *Worker) run() {
	ticker := time.NewTicker(w.flushInterval)

	for {
		select {
		case <-ticker.C:
			w.store()

		case <-w.shutdownSignal:
			ticker.Stop()

			w.shutdownSignal <- struct{}{}

			return
		}
	}
}

func (w *Worker) store() {
	packs := w.buffer.GetAndClear()

	if len(packs) == 0 {
		return
	}

	for _, pack := range packs {
		err := w.storage.Store(pack)

		if err != nil {
			// log error
		}
	}
}
