package entities

type Entity interface {
	Query() string
	Values() []interface{}
}
