package entities

import "gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/events"

type VacancyApplyEventEntity struct {
	events.VacancyApplyEvent
}

func (e VacancyApplyEventEntity) Query() string {
	const (
		// language=SQL
		insertSQL = `INSERT INTO user_vacancy_apply_source_stats (date, time, user_id, vacancy_id)
VALUES (?, ?, ?, ?);`
	)

	return insertSQL
}

func (e *VacancyApplyEventEntity) Values() []interface{} {
	return []interface{}{
		e.Time,
		e.Time,
		e.UserID,
		e.VacancyID,
	}
}
