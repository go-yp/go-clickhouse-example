package entities

type (
	// ORDER BY (date, time, user_id, vacancy_id)
	VacancyHourKey struct {
		Time      int32
		UserID    uint32
		VacancyID uint32
	}

	VacancyHourValue struct {
		ViewCount  uint64
		ApplyCount uint64
	}

	VacancyHour struct {
		VacancyHourKey
		VacancyHourValue
	}
)

func (v VacancyHour) Query() string {
	const (
		// language=SQL
		insertSQL = `INSERT INTO user_vacancy_hour_stats (date, time, user_id, vacancy_id, view_count, apply_count)
VALUES (?, ?, ?, ?, ?, ?);`
	)

	return insertSQL
}

func (v *VacancyHour) Values() []interface{} {
	return []interface{}{
		v.Time,
		v.Time,
		v.UserID,
		v.VacancyID,
		v.ViewCount,
		v.ApplyCount,
	}
}

func (sum VacancyHourValue) Merge(value VacancyHourValue) VacancyHourValue {
	return VacancyHourValue{
		ViewCount:  sum.ViewCount + value.ViewCount,
		ApplyCount: sum.ApplyCount + value.ApplyCount,
	}
}
