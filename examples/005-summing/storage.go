package main

import (
	"database/sql"
	"fmt"
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/entities"
	"log"
)

type Storage struct {
	db *sql.DB
}

func NewStorage(db *sql.DB) *Storage {
	return &Storage{db: db}
}

func (s *Storage) Store(entities []entities.Entity) (err error) {
	if len(entities) == 0 {
		return nil
	}

	var (
		entity    = entities[0]
		insertSQL = entity.Query()
	)

	return execTx(s.db, func(tx *sql.Tx) error {
		stmt, stmtErr := tx.Prepare(insertSQL)
		if stmtErr != nil {
			return stmtErr
		}
		defer stmt.Close()

		for _, entity := range entities {
			_, insertErr := stmt.Exec(entity.Values()...)

			if insertErr != nil {
				log.Printf("insert err %+v", insertErr)

				continue
			}
		}

		return nil
	})
}

func execTx(connect *sql.DB, fn func(*sql.Tx) error) error {
	var tx, txErr = connect.Begin()
	if txErr != nil {
		return txErr
	}

	var err = fn(tx)

	if err != nil {
		if rbErr := tx.Rollback(); rbErr != nil {
			return fmt.Errorf("tx err: %v, rb err: %v", err, rbErr)
		}
		return err
	}

	return tx.Commit()
}
