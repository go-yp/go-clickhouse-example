package events

type VacancyViewEvent struct {
	Time      int32
	UserID    uint32
	VacancyID uint32
}
