package buffer

import (
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/entities"
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/events"
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/summing"
	"sync"
)

type VacancyHourBuffer struct {
	current       map[entities.VacancyHourKey]entities.VacancyHourValue
	complete      []map[entities.VacancyHourKey]entities.VacancyHourValue
	currentLength int
	mu            sync.Mutex
}

func NewVacancyHourBuffer() *VacancyHourBuffer {
	return &VacancyHourBuffer{
		current: make(map[entities.VacancyHourKey]entities.VacancyHourValue),
	}
}

func (b *VacancyHourBuffer) AddVacancyView(event events.VacancyViewEvent) {
	b.add(summing.VacancyView(event))
}

func (b *VacancyHourBuffer) AddVacancyApply(event events.VacancyApplyEvent) {
	b.add(summing.VacancyApply(event))
}

func (b *VacancyHourBuffer) add(key entities.VacancyHourKey, value entities.VacancyHourValue) {
	b.mu.Lock()
	defer b.mu.Unlock()

	if sum, exists := b.current[key]; exists {
		b.current[key] = sum.Merge(value)
	} else {
		b.current[key] = value

		b.currentLength += 1
	}

	if b.isCurrentPackFull() {
		b.completeCurrentPack()
	}
}

func (b *VacancyHourBuffer) GetAndClear() [][]entities.Entity {
	var complete = b.getAndClear()

	var result = make([][]entities.Entity, 0, len(complete))

	for _, keyValueMap := range complete {
		var pack = make([]entities.Entity, 0, len(keyValueMap))

		for key, value := range keyValueMap {
			pack = append(pack, &entities.VacancyHour{key, value})
		}

		result = append(result, pack)
	}

	return result
}

func (b *VacancyHourBuffer) getAndClear() []map[entities.VacancyHourKey]entities.VacancyHourValue {
	b.mu.Lock()
	defer b.mu.Unlock()

	b.completeCurrentPack()

	var complete = b.complete
	b.complete = nil

	return complete
}

func (b *VacancyHourBuffer) isCurrentPackFull() bool {
	return b.currentLength >= packSize
}

func (b *VacancyHourBuffer) completeCurrentPack() {
	if b.currentLength > 0 {
		b.complete = append(b.complete, b.current)

		b.current = make(map[entities.VacancyHourKey]entities.VacancyHourValue, len(b.current))

		b.currentLength = 0
	}
}
