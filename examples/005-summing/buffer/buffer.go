package buffer

import (
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/entities"
	"sync"
)

const (
	packSize = 10000
)

type Buffer struct {
	current       []entities.Entity
	complete      [][]entities.Entity
	currentLength int
	mu            sync.Mutex
}

func NewBuffer() *Buffer {
	return &Buffer{}
}

func (b *Buffer) Add(event entities.Entity) {
	b.mu.Lock()
	defer b.mu.Unlock()

	b.current = append(b.current, event)
	b.currentLength += 1

	if b.isCurrentPackFull() {
		b.completeCurrentPack()
	}
}

func (b *Buffer) GetAndClear() [][]entities.Entity {
	b.mu.Lock()
	defer b.mu.Unlock()

	b.completeCurrentPack()

	var result = b.complete
	b.complete = nil
	return result
}

func (b *Buffer) isCurrentPackFull() bool {
	return b.currentLength >= packSize
}

func (b *Buffer) completeCurrentPack() {
	if b.currentLength > 0 {
		b.complete = append(b.complete, b.current)

		b.current = make([]entities.Entity, 0, packSize)

		b.currentLength = 0
	}
}
