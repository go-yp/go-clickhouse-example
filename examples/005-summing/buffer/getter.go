package buffer

import "gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/entities"

type Getter interface {
	GetAndClear() [][]entities.Entity
}
