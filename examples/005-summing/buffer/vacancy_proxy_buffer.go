package buffer

import (
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/entities"
	"gitlab.com/go-yp/go-clickhouse-example/examples/005-summing/events"
)

type VacancyProxyBuffer struct {
	vacancyViewBuffer  *Buffer
	vacancyApplyBuffer *Buffer
	vacancyHourBuffer  *VacancyHourBuffer
}

func NewVacancyProxyBuffer(vacancyViewBuffer *Buffer, vacancyApplyBuffer *Buffer, vacancyHourBuffer *VacancyHourBuffer) *VacancyProxyBuffer {
	return &VacancyProxyBuffer{vacancyViewBuffer: vacancyViewBuffer, vacancyApplyBuffer: vacancyApplyBuffer, vacancyHourBuffer: vacancyHourBuffer}
}

func (b *VacancyProxyBuffer) AddVacancyView(event events.VacancyViewEvent) {
	b.vacancyViewBuffer.Add(&entities.VacancyViewEventEntity{event})

	b.vacancyHourBuffer.AddVacancyView(event)
}

func (b *VacancyProxyBuffer) AddVacancyApply(event events.VacancyApplyEvent) {
	b.vacancyApplyBuffer.Add(&entities.VacancyApplyEventEntity{event})

	b.vacancyHourBuffer.AddVacancyApply(event)
}
