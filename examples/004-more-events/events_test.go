package main

import (
	"database/sql"
	_ "github.com/ClickHouse/clickhouse-go"
	"gitlab.com/go-yp/go-clickhouse-example/examples/004-more-events/entities"
	"gitlab.com/go-yp/go-clickhouse-example/examples/004-more-events/events"
	"sync/atomic"
	"testing"
	"time"
)

func BenchmarkEvents(b *testing.B) {
	var startTime = time.Now()

	connect, err := sql.Open("clickhouse", "tcp://clickhouse:9000?database=yaaws")
	if err != nil {
		b.Fatal(err)
	}

	if err := connect.Ping(); err != nil {
		b.Fatal(err)
	}

	if err := userOnlineResetTable(connect); err != nil {
		b.Fatal(err)
	}

	if err := vacancyViewResetTable(connect); err != nil {
		b.Fatal(err)
	}

	if err := vacancyApplyResetTable(connect); err != nil {
		b.Fatal(err)
	}

	var (
		storage            = NewStorage(connect)
		userOnlineBuffer   = NewBuffer()
		userOnlineWorker   = NewWorker(userOnlineBuffer, storage, time.Second)
		vacancyViewBuffer  = NewBuffer()
		vacancyViewWorker  = NewWorker(vacancyViewBuffer, storage, time.Second)
		vacancyApplyBuffer = NewBuffer()
		vacancyApplyWorker = NewWorker(vacancyApplyBuffer, storage, time.Second)
	)

	var (
		counter = uint32(time.Now().Add(-7 * 24 * time.Hour).Unix())
	)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var i = atomic.AddUint32(&counter, 1)

			{
				var event = events.UserOnlineEvent{
					Time:   int32(i),
					UserID: uint32(i),
					Role:   uint8(i % 2),
				}

				userOnlineBuffer.Add(&entities.UserOnlineEventEntity{event})
			}

			{
				var event = events.VacancyViewEvent{
					Time:      int32(i),
					UserID:    uint32(i),
					VacancyID: uint32(i % 1024),
				}

				vacancyViewBuffer.Add(&entities.VacancyViewEventEntity{event})
			}

			{
				var event = events.VacancyApplyEvent{
					Time:      int32(i),
					UserID:    uint32(i),
					VacancyID: uint32(i % 1024),
				}

				vacancyApplyBuffer.Add(&entities.VacancyApplyEventEntity{event})
			}
		}
	})

	// for graceful shutdown
	userOnlineWorker.Shutdown()
	vacancyViewWorker.Shutdown()
	vacancyApplyWorker.Shutdown()

	b.Logf("inserted count %10d %s", 3*b.N, time.Since(startTime))
}
