package main

import "database/sql"

const (
	// language=SQL
	userOnlineCreateTableSQL = `CREATE TABLE user_online_source_statistics
(
    date    Date,
    time    DateTime,
    user_id UInt32,
    role    UInt8
) ENGINE = MergeTree()
      PARTITION BY date
      ORDER BY user_id;`

	// language=SQL
	userOnlineDropTableSQL = `DROP TABLE IF EXISTS user_online_source_statistics;`

	// language=SQL
	vacancyViewCreateTableSQL = `CREATE TABLE user_vacancy_view_source_stats
(
    date       Date,
    time       DateTime,
    user_id    UInt32,
    vacancy_id UInt32
) ENGINE = MergeTree()
      PARTITION BY date
      ORDER BY tuple();`

	// language=SQL
	vacancyViewDropTableSQL = `DROP TABLE IF EXISTS user_vacancy_view_source_stats;`

	// language=SQL
	vacancyApplyCreateTableSQL = `CREATE TABLE user_vacancy_apply_source_stats
(
    date       Date,
    time       DateTime,
    user_id    UInt32,
    vacancy_id UInt32
) ENGINE = MergeTree()
      PARTITION BY date
      ORDER BY tuple();`

	// language=SQL
	vacancyApplyDropTableSQL = `DROP TABLE IF EXISTS user_vacancy_apply_source_stats;`
)

func userOnlineResetTable(connect *sql.DB) error {
	return resetTable(connect, userOnlineCreateTableSQL, userOnlineDropTableSQL)
}

func vacancyViewResetTable(connect *sql.DB) error {
	return resetTable(connect, vacancyViewCreateTableSQL, vacancyViewDropTableSQL)
}

func vacancyApplyResetTable(connect *sql.DB) error {
	return resetTable(connect, vacancyApplyCreateTableSQL, vacancyApplyDropTableSQL)
}

func resetTable(connect *sql.DB, createTableSQL, dropTableSQL string) error {
	if _, err := connect.Exec(dropTableSQL); err != nil {
		return err
	}

	if _, err := connect.Exec(createTableSQL); err != nil {
		return err
	}

	return nil
}
