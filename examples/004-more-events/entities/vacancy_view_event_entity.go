package entities

import "gitlab.com/go-yp/go-clickhouse-example/examples/004-more-events/events"

type VacancyViewEventEntity struct {
	events.VacancyViewEvent
}

func (e VacancyViewEventEntity) Query() string {
	const (
		// language=SQL
		insertSQL = `INSERT INTO user_vacancy_view_source_stats (date, time, user_id, vacancy_id)
VALUES (?, ?, ?, ?);`
	)

	return insertSQL
}

func (e *VacancyViewEventEntity) Values() []interface{} {
	return []interface{}{
		e.Time,
		e.Time,
		e.UserID,
		e.VacancyID,
	}
}
