package events

type UserOnlineEvent struct {
	Time   int32
	UserID uint32
	Role   uint8
}
