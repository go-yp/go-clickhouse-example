package events

type VacancyApplyEvent struct {
	Time      int32
	UserID    uint32
	VacancyID uint32
}
