up:
	sudo docker-compose up -d

down:
	sudo docker-compose down

app:
	sudo docker exec -it yaaws_app sh

go-test-run:
	sudo docker exec yaaws_app go run main.go

yaaws-database-create:
	sudo docker exec yaaws_clickhouse clickhouse-client --query "CREATE DATABASE yaaws;"

clickhouse-test-run:
	sudo docker exec yaaws_clickhouse clickhouse-client --query "SELECT VERSION();"

init-test: up go-test-run clickhouse-test-run down

# cgo: exec gcc: exec: "gcc": executable file not found in $PATH
temp-fix-gcc:
	sudo docker exec yaaws_app apk add build-base

example-001-run:
	sudo docker exec yaaws_app go test ./examples/001-clickhouse-user-online-statistics-benchmark/... -v -bench=. -benchmem -benchtime=100s
	sudo docker exec yaaws_clickhouse clickhouse-client --query "SELECT COUNT() FROM yaaws.user_online_source_statistics;"