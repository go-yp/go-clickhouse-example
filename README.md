# Go ClickHouse example

### Стаття на DOU:
 * [Go ClickHouse example](https://dou.ua/forums/topic/34292/)

### Benchmark
```bash
go test ./examples/001-clickhouse-user-online-statistics-benchmark/... -v -bench=. -benchmem -benchtime=100s
```
```text
goos: linux
goarch: amd64
pkg: gitlab.com/go-yp/go-clickhouse-example/examples/001-clickhouse-user-online-statistics-benchmark
cpu: Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz
BenchmarkUserOnlineStatisticsStore
    user_online_statistics_test.go:49: inserted count      10000 by      1 3.756016629s
    user_online_statistics_test.go:49: inserted count    1000000 by    100 250.611035ms
    user_online_statistics_test.go:49: inserted count  100000000 by  10000 22.209494737s
    user_online_statistics_test.go:49: inserted count  540960000 by  54096 2m11.718269866s
BenchmarkUserOnlineStatisticsStore-12    	   54096	   2420397 ns/op	 2850018 B/op	   69824 allocs/op
PASS
ok  	gitlab.com/go-yp/go-clickhouse-example/examples/001-clickhouse-user-online-statistics-benchmark	157.961s
```